import Header from './header.svelte';
import PhrasesList from './phrases-list.svelte';

export * from './phrase-translations';
export { Header, PhrasesList };
